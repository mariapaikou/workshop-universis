import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { MessagesModule } from './messages.module';
describe('MessagesModule', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ]
    }).compileComponents();
  }));
  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const messagesModule = new MessagesModule(translateService);
    expect(messagesModule).toBeTruthy();
  }));
});

